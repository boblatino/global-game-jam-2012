#include "FXManager.h"

#include <cassert>
#include <IBillboardSceneNode.h>
#include <IMeshManipulator.h>
#include <IMeshSceneNode.h>
#include <IParticleSystemSceneNode.h>
#include <IrrlichtDevice.h>
#include <ISceneManager.h>

#include <game/EntitiesID.h>

namespace utils
{
    FXManager::FXManager() 
        : mPS(nullptr)
        , mEmitter(nullptr)
        , mAffector(nullptr)
        , mExplosion(nullptr)
    {

    }

    FXManager::~FXManager() {}

    // Load interface function for manager
    bool FXManager::load(irr::IrrlichtDevice * const device)
    {
        assert(device && "FXManager::Load: NULL pointer");

        loadExplosion(device);

        return true;
    }

    // Adds a specified particle effect to the game world
    bool FXManager::addParticleEffect(irr::IrrlichtDevice * const device, ParticleEffect * const effect)
    {
        assert(device && "FXManager::AddParticleEffect: NULL pointer");
        assert(effect && "FXManager::AddParticleEffect: NULL pointer");

        // get pointer to Particle System
        mPS = device->getSceneManager()->addParticleSystemSceneNode(false, 0, -1);
        mPS->setPosition(effect->getPosition());
        mPS->setID(effect->getID());
        mPS->setScale(irr::core::vector3df(effect->getScaleX(), effect->getScaleY(), effect->getScaleZ()));

        // set up a box emmiter
        mEmitter = mPS->createBoxEmitter(irr::core::aabbox3d<irr::f32>(-7, 0, -7, 7, 1, 7), irr::core::vector3df(0.0f, 0.03f, 0.0f),
            effect->getMinParticlesPerSecond(), 	effect->getMaxParticlesPerSecond(), 	effect->getStartColor(), effect->getEndColor(),
            effect->getLifetimeMin(), effect->getLifetimeMax());

        const irr::core::dimension2d<irr::f32> aux(effect->getParticleSize());
        mEmitter->setMinStartSize(aux);
        mEmitter->setMaxStartSize(aux);

        mPS->setEmitter(mEmitter);
        mEmitter->drop();

        // Fade out particle affector
        mAffector = mPS->createFadeOutParticleAffector();
        mPS->addAffector(mAffector);
        mAffector->drop();

        // if gravity effects specified, add gravity effector
        if (effect->getTimeForce() > 0) 
        {
            mAffector= mPS->createGravityAffector (effect->getGravity() ,effect->getTimeForce());
            mPS->addAffector(mAffector);
            mAffector->drop();
        }

        mPS->setMaterialFlag(irr::video::EMF_LIGHTING, false);
        mPS->setMaterialTexture(0, device->getVideoDriver()->getTexture(effect->getSprite()));
        mPS->setMaterialType(irr::video::EMT_TRANSPARENT_VERTEX_ALPHA);

        return true;
    }

    // Adds a specified particle effect to the game world, overrides position of effect
    bool FXManager::addParticleEffect(irr::IrrlichtDevice * const device, ParticleEffect * const effect, const irr::core::vector3df &pos)
    {
        assert(device && "FXManager::AddParticleEffect: NULL pointer");
        assert(effect && "FXManager::AddParticleEffect: NULL pointer");

        effect->setPosition(pos);

        return addParticleEffect(device, effect);
    }	

    // Adds a specified particle effect to a model
    bool FXManager::addParticleEffect(irr::IrrlichtDevice * const device, ParticleEffect * const effect, irr::scene::IAnimatedMeshSceneNode * const model)
    {
        assert(device && "FXManager::AddParticleEffect: NULL pointer");
        assert(effect && "FXManager::AddParticleEffect: NULL pointer");

        addParticleEffect(device, effect);
        model->addChild(mPS);

        return true;
    }

    // creates particle effect for bullets hiting walls, enemies etc. A different colour sprite
    // can be passed in as a parameter depending on blood, shrapnel etc.
    irr::scene::IParticleSystemSceneNode* FXManager::createGenericParticleEffect(irr::IrrlichtDevice * const device, 
        const irr::core::vector3df &position, const irr::c8 * const sprite, const uint32_t time, const uint32_t size)
    {
        assert(device && "FXManager::CreateGenericParticleEffect: NULL pointer");
        assert(sprite && "FXManager::CreateGenericParticleEffect: NULL pointer");

        irr::scene::IParticleSystemSceneNode *ps;
        ps = device->getSceneManager()->addParticleSystemSceneNode(false);
        ps->setMaterialFlag(irr::video::EMF_LIGHTING, false);
        ps->setPosition(position);
        ps->setMaterialTexture(0, device->getVideoDriver()->getTexture(sprite));
        ps->setMaterialType(irr::video::EMT_TRANSPARENT_VERTEX_ALPHA);
        irr::scene::IParticleEmitter *emb = ps->createBoxEmitter(irr::core::aabbox3d<irr::f32>(-7.0f, 0.0f, -7.0f, 7.0f, 1.0f, 7.0f),
            irr::core::vector3df(0.0f, 0.03f, 0.0f), 40, 165, irr::video::SColor(0, 64, 91, 100), irr::video::SColor(0, 255, 255, 255), 100,900,237);

        emb->setMinStartSize(irr::core::dimension2d<irr::f32>(static_cast<irr::f32>(size), static_cast<irr::f32>(size * 2)));
        emb->setMaxStartSize(irr::core::dimension2d<irr::f32>(static_cast<irr::f32>(size), static_cast<irr::f32>(size * 2)));
        ps->setEmitter(emb);
        emb->drop();
        irr::scene::IParticleAffector *paf = ps->createFadeOutParticleAffector(irr::video::SColor(0,0,0,0),1000);
        ps->addAffector(paf);
        paf->drop();
        irr::scene::IParticleAffector *pgaf = ps->createGravityAffector(irr::core::vector3df(0.00f,-0.03f,0.00f),850);	ps->addAffector(pgaf);
        pgaf->drop();

        irr::scene::ISceneNodeAnimator *anim = device->getSceneManager()->createDeleteAnimator(time);
        ps->addAnimator(anim);
        anim->drop();

        return ps;
    }

    // Add 'cheap water' using a HillPlaneMesh using basic parameters
    void FXManager::createWaterEffectNode(irr::IrrlichtDevice * const device, const irr::core::vector3df &position)
    {
        assert(device && "FXManager::CreateWaterEffectNode: NULL pointer");

        createWaterEffectNode(device, position, irr::core::dimension2d<irr::f32>(40, 40), "media/waterfloor.jpg", "media/water.jpg");
    }

    // Add 'cheap water' using a HillPlaneMesh using adittional parameters, size, layers etc
    void FXManager::createWaterEffectNode(irr::IrrlichtDevice * const device, const irr::core::vector3df &position, 
        const irr::core::dimension2d<irr::f32> &size, const irr::c8 * const layer1, const irr::c8 * const layer2)
    {
        assert(device && "FXManager::CreateWaterEffectNode: NULL pointer");

        irr::scene::ISceneNode *waterNode = nullptr;

        irr::core::dimension2d<uint32_t> a(40, 40);
        irr::core::dimension2d<irr::f32> b(0, 0);
        irr::core::dimension2d<irr::f32> c(10, 10);

        device->getSceneManager()->addHillPlaneMesh("waterMesh", size, a, 0, 0, b, c);

        // wave size and speed can be modified here
        waterNode = device->getSceneManager()->addWaterSurfaceSceneNode(device->getSceneManager()->getMesh("waterMesh")->getMesh(0), 3.0f, 300.0f, 30.0f);
        waterNode->setID(game::EntitiesIds::mWaterNodeId);
        waterNode->setPosition(position);
        waterNode->setMaterialTexture(0, device->getVideoDriver()->getTexture(layer1));
        waterNode->setMaterialTexture(1, device->getVideoDriver()->getTexture(layer2));
        waterNode->setMaterialType(irr::video::EMT_REFLECTION_2_LAYER); 
    }

    // create a Quick Bump map node using minimal parameters, suitable for testing
    irr::scene::ISceneNode* FXManager::createBumpMapNode(irr::IrrlichtDevice * const device, irr::scene::IAnimatedMesh * const mesh)
    {
        assert(device && "FXManager::CreateBumpMapNode: NULL pointer");
        assert(mesh && "FXManager::CreateBumpMapNode: NULL pointer");

        return createBumpMapNode(device, mesh, "media/rockwall.bmp", "media/rockwall_height.bmp", irr::video::EMT_PARALLAX_MAP_SOLID);                                  
    }

    // Returns a Bump maped node based on supplied parameters. Types are :
    // EMT_SOLID
    // EMT_NORMAL_MAP_SOLID
    // EMT_PARALLAX_MAP_SOLID    
    irr::scene::ISceneNode* FXManager::createBumpMapNode(irr::IrrlichtDevice * const device, irr::scene::IAnimatedMesh * const mesh, 
        const irr::c8 * const colorMapName, const irr::c8 * const heightMapName, const irr::video::E_MATERIAL_TYPE type)
    {
        assert(device && "FXManager::CreateBumpMapNode: NULL pointer");
        assert(mesh && "FXManager::CreateBumpMapNode: NULL pointer");
        assert(colorMapName && "FXManager::CreateBumpMapNode: NULL pointer");
        assert(heightMapName && "FXManager::CreateBumpMapNode: NULL pointer");

        device->getSceneManager()->getMeshManipulator()->makePlanarTextureMapping(mesh->getMesh(0), 0.003f);  

        // The normal map texture is being generated by the makeNormalMapTexture method
        irr::video::ITexture *colorMap = device->getVideoDriver()->getTexture(colorMapName);
        irr::video::ITexture *normalMap = device->getVideoDriver()->getTexture(heightMapName);
        device->getVideoDriver()->makeNormalMapTexture(normalMap, 9.0f);

        // Create tangent map
        irr::scene::IMesh *tangentMesh = device->getSceneManager()->getMeshManipulator()->createMeshWithTangents(mesh->getMesh(0));					
        irr::scene::ISceneNode *node = device->getSceneManager()->addMeshSceneNode(tangentMesh);		
        node->setMaterialTexture(0,	colorMap);		
        node->setMaterialTexture(1,	normalMap);		
        node->getMaterial(0).EmissiveColor.set(0,0,0,0);		
        node->setMaterialType(type); 		
        node->getMaterial(0).MaterialTypeParam = 0.02f; 	
        tangentMesh->drop();  

        return node;
    }

    // Create an Explosion based on a series of bitmaps, animated sprites
    void FXManager::loadExplosion(irr::IrrlichtDevice * const device)
    {
        assert(device && "FXManager::LoadExplosion: NULL pointer");

        // load in sprites for explosion
        for (uint32_t d = 0; d < 8; ++d)
        {
            char tmp[64];

            sprintf_s(tmp, "media/sprites/explode%d.pcx", d);
            irr::video::ITexture* t = device->getVideoDriver()->getTexture(tmp);

            // Add one more sprite in the array
            mSprites.push_back(t);
        }
    }

    // Create an Explosion based on a series of bitmaps, animated sprites
    void FXManager::createExplosion(irr::IrrlichtDevice * const device, const irr::core::vector3df &pos)
    {
        assert(device && "FXManager::CreateExplosion: NULL pointer");

        irr::scene::ISceneNodeAnimator *anim = nullptr;
        anim = device->getSceneManager()->createTextureAnimator(mSprites, 100);

        // create sprite
        mExplosion = device->getSceneManager()->addBillboardSceneNode(0, irr::core::dimension2d<irr::f32>(100,100), pos);
        mExplosion->setMaterialFlag(irr::video::EMF_LIGHTING, false);
        mExplosion->setMaterialTexture(0, device->getVideoDriver()->getTexture("media/sprites/explode0.pcx"));
        mExplosion->setMaterialType(irr::video::EMT_TRANSPARENT_ADD_COLOR);
        mExplosion->addAnimator(anim);
        anim->drop();

        // add a delete animator also
        irr::scene::ISceneNodeAnimator *animDelete = device->getSceneManager()->createDeleteAnimator(500);
        mExplosion->addAnimator(animDelete);
        animDelete->drop();
    }

}