#pragma once

#include <cstdint>

namespace game
{
    // Static class which has specific entities id's
    class EntitiesIds
    {
    public:
        static const uint32_t mEnemyId = 0;
        static const uint32_t mPlayerId = 1;
        static const uint32_t mMapId = 2;
        static const uint32_t mDoorId = 3;
        static const uint32_t mWaterNodeId = 4;
        static const uint32_t mMunitionIdMin = 99;
        static const uint32_t mMunitionIdMax = 199;
        static const uint32_t mHealthIdMin = 200;
        static const uint32_t mHealthIdMax = 299;
        static const uint32_t mVenomIdMin = 300;
        static const uint32_t mVenomIdMax = 399;
        static const uint32_t mBlueKeyMin = 400;
        static const uint32_t mBlueKeyMax = 499;
        static const uint32_t mRedKeyMin = 500;
        static const uint32_t mRedKeyMax = 599;
        static const uint32_t mGreenKeyMin = 600;
        static const uint32_t mGreenKeyMax = 699;
        static const uint32_t mVioletKeyMin = 700;
        static const uint32_t mVioletKeyMax = 799;


    private:
        EntitiesIds();
        ~EntitiesIds();
        EntitiesIds(const EntitiesIds& entitiesIds);
        EntitiesIds& operator=(const EntitiesIds& entitiesIds);
    };
}