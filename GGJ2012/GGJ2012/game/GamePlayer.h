#pragma once

#include <cstdint>

#include <vector3d.h>

#include <game/IGameEntity.h>

namespace irr
{
	namespace scene
	{
		class ICameraSceneNode;
	}
}

namespace game
{
    class GamePlayer : public game::IGameEntity
    {
    public:
        GamePlayer(const uint32_t id) 
            : IGameEntity(id) 
            , mLockPosition(irr::core::vector3df(0.0f, 0.0f, 0.0f))
            , mLockRotation(irr::core::vector3df(0.0f, 0.0f, 0.0f))
            , mHealth(0)
            , mMaxHealth(0)
            , mDeathTime(0)
            , mInitDeathTime(true)
        {

        }

        void init(const uint32_t lives, const uint32_t health, const uint32_t maxHealth);
        void update() {}
        bool kill();

        uint32_t health() const { return mHealth; }

        void setHealth(const uint32_t health) 
        { 
            mHealth = (health >= mMaxHealth) ? mMaxHealth : health; 
        }

        uint32_t maxHealth() const { return mMaxHealth; }

        void setMaxHealth(const uint32_t health)
        {
            mMaxHealth = health;

            if(health < mHealth)
                mHealth = health;
        }

    private:
        static const uint32_t smWaitingTime = 5000; // Time to wait before death completion.
        
        irr::scene::ICameraSceneNode* fixPlayerCam(); 

        irr::core::vector3df mLockPosition;
        irr::core::vector3df mLockRotation;

        uint32_t mHealth;
        uint32_t mMaxHealth;

        uint32_t mDeathTime; // Time when the player dies. (This is verified inside Death() function)

        bool mInitDeathTime; // Init death time inside Death() function
    };
}