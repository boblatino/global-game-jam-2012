#include "GameManager.h"

#include <ICursorControl.h>
#include <IGUIEnvironment.h>
#include <ISceneManager.h>
#include <ITimer.h>
#include <IVideoDriver.h>

#include <engine/IrrlichtGlobals.h>

#include <gameStates/IGameState.h>

namespace game
{
    void GameManager::update()
    {
        irr::ITimer& timer = IrrlichtGlobals::timer(); 
        uint32_t timeThisFrame = timer.getTime(); 

        // Lock FPS at around 60 
        while(timeThisFrame - mTimePreviousFrame <= 16) 
        { 
            timer.tick(); 
            timeThisFrame = timer.getTime(); 
        } 

        mTimePreviousFrame = timeThisFrame;

        mGameState->update();
    }

    // Main event handler derived from IEventHandler, this 
    // will be passed down to the current states keyboard handler.
    bool GameManager::OnEvent(const irr::SEvent &event)
    {
        if(mGameState)
        {
            mGameState->keyboardEvent(event);
            mGameState->mouseEvent(event);
        }

        return false;
    }

    // Changes the game state
    void GameManager::changeState(gameStates::IGameState &state)
    {
        // Call exit state function
        if (mGameState)
            mGameState->clear();

        // I verify that the state is a new one.
        if (&state != mGameState)
        {
            mGameState = &state;

            // Call enter state function
            mGameState->init();
        }
    }

    GameManager& gameManager()
    {
        static GameManager gameMgr;

        return gameMgr;
    }
}