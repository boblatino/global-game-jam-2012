#include "Game.h"

#include <irrlichtDevice.h>

#include <game/GameManager.h>

#include <engine/IrrlichtGlobals.h>

namespace game
{
    // Main game loop
    void Game::run()
    {
        IrrlichtGlobals::initAll();
        game::gameManager().init();

        // Keep running game loop if device exists
        irr::IrrlichtDevice& irrlichtDevice = IrrlichtGlobals::device();
        while(irrlichtDevice.run())
        {	 
            if (irrlichtDevice.isWindowActive())
                game::gameManager().update();
        }

        game::gameManager().clear();
        IrrlichtGlobals::destroyAll();
    }
}