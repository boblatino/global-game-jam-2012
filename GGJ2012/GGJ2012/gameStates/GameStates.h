#pragma once

namespace gameStates
{
    class CreditsState;
    class ControlsMenuState;
    class IntroState;
    class LevelState;
    class LoadingState;
    class MenuState;

    // Static class to access globally to game states.
    class GameStates
    {
    public:
        static void initAll();
        static void destroyAll();

        static CreditsState& creditsState() { return *mCreditsState; }
        static ControlsMenuState& controlsMenuState() { return *mControlsMenuState; }
        static IntroState& introState() { return *mIntroState; }
        static LevelState& levelState() { return *mLevelState; }
        static LoadingState& loadingState() { return *mLoadingState; }
        static MenuState& menuState() { return *mMenuState; }

    private:
        GameStates();
        ~GameStates();
        GameStates(const GameStates& gameStates);

        GameStates& operator=(const GameStates& gameStates);

        static CreditsState* mCreditsState;
        static ControlsMenuState* mControlsMenuState;
        static IntroState* mIntroState;
        static LevelState* mLevelState;
        static LoadingState* mLoadingState;
        static MenuState* mMenuState;
    };
}