#pragma once

#include <vector2d.h>

#include <IGUIImage.h>

#include <gameStates/IGameState.h>

namespace irr
{
	struct SEvent;

	namespace gui
	{
		class IGUIStaticText;
	}

	namespace video
	{
		class ITexture;
	}
}

namespace gameStates
{
    // Menu where we can see game controls.
    class ControlsMenuState : public IGameState
    {
    public:
        ControlsMenuState() 
            : IGameState()
            , mMousePos(irr::core::vector2d<int>(0, 0))
            , mMouseSensibility(50.0f)
            , mMouseCursor(nullptr)
            , mControlsText(nullptr)
            , mExitButtonImage(nullptr)
            , mExitButtonHighImage(nullptr)
        {

        }

        void init();
        void update();
        void clear();

        void keyboardEvent(const irr::SEvent &event);
        void mouseEvent(const irr::SEvent &event);

        float getMouseSensibility(void) const { return mMouseSensibility; }

    private:
        void mouseClicked();
        void mouseOver();

        void resetButtons()
        {
            // set highlighted button to off
            mExitButtonHighImage->setVisible(false);

            // set normal buttons on
            mExitButtonImage->setVisible(true);
        }
        
        void displayMouse();
        void loadMouseCursor();

        irr::core::vector2d<int> mMousePos;
        float mMouseSensibility;

        irr::video::ITexture *mMouseCursor;
        irr::gui::IGUIStaticText *mControlsText;
        irr::gui::IGUIImage *mExitButtonImage;
        irr::gui::IGUIImage *mExitButtonHighImage;
    };
}