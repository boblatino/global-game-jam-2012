#include "CreditsState.h"

#include <IGUIEnvironment.h>
#include <IGUIStaticText.h>
#include <IrrlichtDevice.h>

#include <game/GameManager.h>

#include <engine/IrrlichtGlobals.h>

#include <gameStates/GameStates.h>
#include <gameStates/MenuState.h>

namespace gameStates
{
    void CreditsState::init()
    {	
        mPosition = 0;
        mLastScrollingTime = 0;

        addCredits();
        fadeInOut();
    }

    void CreditsState::update()
    {
        // Clear the screen to black.
        IrrlichtGlobals::videoDriver().beginScene(true, true, irr::video::SColor(0, 0, 0, 0));

        IrrlichtGlobals::guiEnvironment().drawAll();	

        IrrlichtGlobals::videoDriver().endScene();

        // Check if we must scroll the credits.
        const uint32_t currentTime = IrrlichtGlobals::timer().getTime();
        if(currentTime - mLastScrollingTime > mScrollingDeltaTime)
        {
            scrollCredits();
            mLastScrollingTime = currentTime;
        }
    }

    void CreditsState::clear()
    {
        IGameState::clear();

        mCredits.clear();

        // Clear CreditItems
        for (uint16_t i = 0; i < smCreditSize; ++i) 
            mCreditItems[i]->remove();

        mCreditItems.clear();
    }

    void CreditsState::keyboardEvent(const irr::SEvent &event)
    {
        if (event.EventType == irr::EET_KEY_INPUT_EVENT)
        {
            // A key was pressed down.
            if(event.KeyInput.PressedDown)
            {
                if(event.KeyInput.Key == irr::KEY_ESCAPE)
                    game::gameManager().changeState(GameStates::menuState());
            }
        }
    }

    // Mouse event handler passed down from GameManager
    void CreditsState::mouseEvent(const irr::SEvent &event)
    {
        // Mouse event.
        if (event.EventType == irr::EET_MOUSE_INPUT_EVENT)
        {
            if(event.MouseInput.Event == irr::EMIE_LMOUSE_LEFT_UP)
                game::gameManager().changeState(GameStates::menuState());
        }
    }

    // Adds GUI Static text objects to an Array of credits (in reverse order)
    void CreditsState::addCredits()
    {	
        wchar_t *credit;
        credit = L"              www.findsounds.com";
        mCredits.push_back(credit);
        credit = L"              irrlicht.sourceforge.net";
        mCredits.push_back(credit);
        credit = L"              S O U N D";
        mCredits.push_back(credit);
        credit = L"";
        mCredits.push_back(credit);
        credit = L"              www.noderunner.net";
        mCredits.push_back(credit);
        credit = L"              irrwizard.sourceforge.net";
        mCredits.push_back(credit);
        credit = L"              3D Map and Models:";
        mCredits.push_back(credit);
        credit = L"";
        mCredits.push_back(credit);
        credit = L"              Mariano Bertoa";
        mCredits.push_back(credit);
        credit = L"              Menu Design:";
        mCredits.push_back(credit);
        credit = L"              A R T";
        mCredits.push_back(credit);
        credit = L"";
        mCredits.push_back(credit);
        credit = L"              Nicolas Bertoa (Nikko_Bertoa)";
        mCredits.push_back(credit);
        credit = L"              Additional Programming:";
        mCredits.push_back(credit);
        credit = L"";
        mCredits.push_back(credit);
        credit = L"              irrlicht.sourceforge.net";
        mCredits.push_back(credit);
        credit = L"              Irrlicht Graphic Engine:";
        mCredits.push_back(credit);
        credit = L"";
        mCredits.push_back(credit);
        credit = L"              irrwizard.sourceforge.net";
        mCredits.push_back(credit);
        credit = L"              FPS Framework IrrWizard:";
        mCredits.push_back(credit);
        credit = L"              P R O G R A M M I N G";
        mCredits.push_back(credit);
        credit = L"";
        mCredits.push_back(credit);

        // now add to list
        loadCredits();	
    }

    // Load credits uint32_to list
    void CreditsState::loadCredits()
    {
        const irr::core::rect<int> rect(smLeftMargin, 0, smRightMargin, 200);
        irr::gui::IGUIEnvironment& gui = IrrlichtGlobals::guiEnvironment();
        for(uint16_t i = 0; i < smCreditSize; ++i) 
        {
            irr::gui::IGUIStaticText *creditItem = gui.addStaticText(mCredits[i], irr::core::rect<int>(smLeftMargin, 0, smRightMargin, 200), false); 
            creditItem->setOverrideColor(irr::video::SColor(128, 200, 200, 200));
            creditItem->setOverrideFont(gui.getFont("media/fonts/font_gothic18.bmp"));
            creditItem->setVisible(true);
            mCreditItems.push_back(creditItem);
        }
    }

    // Scrolls through the list of credits
    void CreditsState::scrollCredits()
    {
        ++mPosition;

        for(uint16_t i = 0; i < smCreditSize; ++i) 
        {
            irr::gui::IGUIStaticText * const creditItem = mCreditItems[i];
            creditItem->setRelativePosition(irr::core::rect<int>(smLeftMargin,smEndCredits-(mPosition + (i * smLineSpace)),
                smRightMargin,smEndCredits-(mPosition + (i * smLineSpace)) + 120));
        }

        if (mPosition > smEndCredits)
            game::gameManager().changeState(GameStates::menuState());
    }
}