#pragma once

#include <engine/Globals.h>
#include <gameStates/IGameState.h>

namespace gameStates
{
    class LevelState : public IGameState 
    {
    public:
        LevelState() 
            : IGameState()
        {

        }

    public:
        void init();
        void update();
        void clear();
        void keyboardEvent(const irr::SEvent &event);
        void mouseEvent(const irr::SEvent &event);

    private:        
        void initPlayer();        
    };
}