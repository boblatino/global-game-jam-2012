#include "LevelState.h"

#include <IBillboardSceneNode.h>
#include <ICameraSceneNode.h>
#include <IGUIEnvironment.h>
#include <IGUIStaticText.h>
#include <ik_ISoundEngine.h>
#include <IMeshSceneNode.h>
#include <irrlichtDevice.h>
#include <ISceneManager.h>
#include <ISceneNodeAnimatorCollisionResponse.h>

#include <game/EntitiesID.h>
#include <game/GameManager.h>
#include <game/GamePlayer.h>

#include <engine/IrrlichtGlobals.h>

#include <gameStates/ControlsMenuState.h>
#include <gameStates/CreditsState.h>
#include <gameStates/MenuState.h>

#include <sound/SoundEngine.h>


namespace gameStates
{
    // Initialization, loads data required for level
    void LevelState::init()
    {
        Globals::initAll();
        sound::SoundEngine::init();      
    }

    // Update, game loop for level, updates, moves and
    // displays all elements used by level
    void LevelState::update()
    {
        // Clear the screen
        irr::video::IVideoDriver& videoDrv = IrrlichtGlobals::videoDriver();
        videoDrv.beginScene(true, true, irr::video::SColor(0, 100, 100, 100));

        IrrlichtGlobals::sceneManager().drawAll();	
        IrrlichtGlobals::guiEnvironment().drawAll();
        
        game::gameManager().changeState(GameStates::creditsState());
    }

    // Clears or destroys resources used by level, clean up.
    void LevelState::clear()
    {       
        IrrlichtGlobals::sceneManager().clear();

        sound::SoundEngine::destroy();

        Globals::destroyAll();
    }

    // Keyboard event for level (state), main keyboard events
    // passed down by Game manager
    void LevelState::keyboardEvent(const irr::SEvent &event)
    {
        // Keyboard event.
        if (event.EventType == irr::EET_KEY_INPUT_EVENT)
        {
            // A key was pressed down.
            if(event.KeyInput.PressedDown)
            {
                // Pass input down to the specific game state keyboard handler
                if(event.KeyInput.Key == irr::KEY_ESCAPE)
                    game::gameManager().changeState(GameStates::menuState());
            }
        }
    }

    // Mouse event handler passed down by GameManager
    void LevelState::mouseEvent( const irr::SEvent &event)
    {
        // Mouse event.
        if (event.EventType == irr::EET_MOUSE_INPUT_EVENT)
        {
            switch(event.MouseInput.Event) 
            { 
            case irr::EMIE_LMOUSE_PRESSED_DOWN:	
                break;

                // Change weapon 
            case irr::EMIE_MOUSE_WHEEL:			
                break; 
            }
        }
    }

    void LevelState::initPlayer()
    {       
        // Init player.
        Globals::gamePlayer().init(3, 100, 100); 
    }
}
