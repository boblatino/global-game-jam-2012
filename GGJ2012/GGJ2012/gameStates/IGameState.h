//////////////////////////////////////////////////////////////////////////
//  The GameState object comprises of the following main elements:
//	
//  1. keyboardEvent - Passed down from the GameManager's OnEvent handler
//                     that has been inherited from the IEventReceiver
//
//	2. init          - Called once at the start of a game state. Performs 
//                     initialization of the game state (Game Level), ie
//					 Irrlicht specific tasks, GUI elements, loading models etc 	
//	
//  3. update        - Called each frame, renders the screen. Updates Irrlicht
//                     ISceneManager, IGUIEnvironment etc. Additional calls can 
//                     be made for things such as particle effects, collision 
//                     detection or AI.
//
//  4. clear         - Called once at the end of each game state for tidy up 
//                     tasks, deleting unused resources etc.  
//
//////////////////////////////////////////////////////////////////////////

#pragma once

namespace irr
{
	namespace gui
	{
		class IGUIInOutFader;
	}

	struct SEvent;
}

namespace gameStates
{
    class IGameState  
    {
    public:
        IGameState() 
            : mInOutFader(nullptr)
        {

        }

        virtual void init() = 0;
        virtual void update() = 0;
        virtual void clear() { mInOutFader = nullptr; }

        virtual void keyboardEvent(const irr::SEvent &event) = 0;
        virtual void mouseEvent(const irr::SEvent &event) = 0;

    protected:
        void fadeInOut();

        irr::gui::IGUIInOutFader *mInOutFader;
    };
}