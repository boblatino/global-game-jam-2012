#include "GameStates.h"

#include <gameStates/CreditsState.h>
#include <gameStates/ControlsMenuState.h>
#include <gameStates/IntroState.h>
#include <gameStates/LevelState.h>
#include <gameStates/LoadingState.h>
#include <gameStates/MenuState.h>

namespace gameStates
{
    CreditsState* GameStates::mCreditsState = nullptr; 
    ControlsMenuState* GameStates::mControlsMenuState = nullptr; 
    IntroState* GameStates::mIntroState = nullptr; 
    LevelState* GameStates::mLevelState = nullptr; 
    LoadingState* GameStates::mLoadingState = nullptr; 
    MenuState* GameStates::mMenuState = nullptr; 

    void GameStates::initAll()
    {
        mCreditsState = new CreditsState();
        mControlsMenuState = new ControlsMenuState();
        mIntroState = new IntroState();
        mLevelState = new LevelState();
        mLoadingState = new LoadingState();
        mMenuState = new MenuState();
    }

    void GameStates::destroyAll()
    {
        delete mCreditsState;
        delete mControlsMenuState;
        delete mIntroState;
        delete mLevelState;
        delete mLoadingState;
        delete mMenuState;
    }
}