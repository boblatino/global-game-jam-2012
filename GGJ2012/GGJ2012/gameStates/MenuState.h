#pragma once

#include <vector2d.h>

#include <gameStates/IGameState.h>

namespace irr
{
	namespace gui
	{
		class IGUIImage;
	}

	namespace video
	{
		class ITexture;
	}
}

namespace gameStates
{
    class MenuState : public IGameState
    {
    public:
        MenuState() 
            : IGameState()
            , mMousePos(0, 0)
            , mMouseCursor(nullptr)
            , mIntroImage(nullptr)
            , mPlayButtonImage(nullptr)
            , mPlayButtonHighImage(nullptr)
            , mCreditsButtonImage(nullptr)
            , mCreditsButtonHighImage(nullptr)
            , mOptionsButtonImage(nullptr)
            , mOptionsButtonHighImage(nullptr)
            , mExitButtonImage(nullptr)
            , mExitButtonHighImage(nullptr) 
        {

        }

        void init();
        void update();
        void clear();

        void keyboardEvent(const irr::SEvent &event);
        void mouseEvent(const irr::SEvent &event);

    private:
        void mouseClicked();
        void mouseOver();
        void resetMenuButtons();
        void displayMouse();
        void loadMouseCursor();
        void initMenuButtons();

        irr::core::vector2d<int> mMousePos; // Position of the mouse cursor.
        irr::video::ITexture *mMouseCursor; // Image of the mouse cursor.

        // Images to display in the main menu.
        irr::gui::IGUIImage *mIntroImage;
        irr::gui::IGUIImage *mPlayButtonImage;
        irr::gui::IGUIImage *mPlayButtonHighImage;
        irr::gui::IGUIImage *mCreditsButtonImage;
        irr::gui::IGUIImage *mCreditsButtonHighImage;
        irr::gui::IGUIImage *mOptionsButtonImage;
        irr::gui::IGUIImage *mOptionsButtonHighImage;
        irr::gui::IGUIImage *mExitButtonImage;
        irr::gui::IGUIImage *mExitButtonHighImage;
    };
}