#include "LoadingState.h"

#include <IGUIEnvironment.h>
#include <ITimer.h>
#include <IVideoDriver.h>

#include <game/GameManager.h>

#include <engine/IrrlichtGlobals.h>

#include <gameStates/GameStates.h>
#include <gameStates/LevelState.h>

namespace gameStates
{
    void LoadingState::init()
    {
        // Set start time of the state.
        mStateStartTime = IrrlichtGlobals::timer().getTime();

        // Set 'loading...' text
        irr::gui::IGUIEnvironment& gui = IrrlichtGlobals::guiEnvironment();
        mTextItem = gui.addStaticText(L"Loading...", irr::core::rect<int>(460, 364, 800, 428), false); 
        mTextItem->setOverrideColor(irr::video::SColor(128, 200, 200, 200));
        mTextItem->setOverrideFont(gui.getFont("media/fonts/font_gothic18.bmp"));
        mTextItem->setVisible(true);

        fadeInOut();
    }

    void LoadingState::update()
    {
        // Clear the screen to black.
        irr::video::IVideoDriver& videoDrv = IrrlichtGlobals::videoDriver();
        videoDrv.beginScene(true, true, irr::video::SColor(0, 0, 0, 0));

        IrrlichtGlobals::guiEnvironment().drawAll();			

        videoDrv.endScene();

        // If this state finished its period of time, then we change to level state.
        if(IrrlichtGlobals::timer().getTime() - mStateStartTime > mDeltaTimeToChangeState)
            game::gameManager().changeState(GameStates::levelState());  
    }
}