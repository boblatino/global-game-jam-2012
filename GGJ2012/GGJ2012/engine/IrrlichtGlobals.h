#pragma once

namespace irr
{
    class IrrlichtDevice;
    class ITimer;
    
    namespace gui 
    {
        class ICursorControl;
        class IGUIEnvironment;
    }

    namespace io 
    {
        class IFileSystem;
    }

    namespace video
    {
        class IVideoDriver;
    }

    namespace scene
    {
        class ISceneCollisionManager;
        class ISceneManager;
    }
}

class IrrlichtGlobals
{
public:
    static void initAll();
    static void destroyAll();

    static irr::IrrlichtDevice& device() {return *mDevice;}

    static irr::gui::IGUIEnvironment& guiEnvironment() {return *mGuiEnvironment;}

    static irr::video::IVideoDriver& videoDriver() {return *mVideoDriver;}

    static irr::ITimer& timer() {return *mTimer;}

    static irr::gui::ICursorControl& cursorControl() {return *mCursorControl;}

    static irr::scene::ISceneManager& sceneManager() {return *mSceneManager;}

    static irr::io::IFileSystem& fileSystem() {return *mFileSystem;}

    static irr::scene::ISceneCollisionManager& collisionManager() {return *mCollisionManager;}

private:
    static irr::IrrlichtDevice* mDevice;
    static irr::gui::IGUIEnvironment* mGuiEnvironment;
    static irr::video::IVideoDriver* mVideoDriver;
    static irr::ITimer* mTimer;
    static irr::gui::ICursorControl* mCursorControl;
    static irr::scene::ISceneManager* mSceneManager;
    static irr::io::IFileSystem* mFileSystem;
    static irr::scene::ISceneCollisionManager* mCollisionManager;

    IrrlichtGlobals();
    IrrlichtGlobals(const IrrlichtGlobals& irrlichtGlobals);
    IrrlichtGlobals& operator=(const IrrlichtGlobals& irrlichtGlobals);
};