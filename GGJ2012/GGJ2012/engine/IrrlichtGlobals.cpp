#include "IrrlichtGlobals.h"

#include <cstdint>

#include <irrlicht.h>
#include <IrrlichtDevice.h>

#include <game/GameManager.h>

irr::IrrlichtDevice* IrrlichtGlobals::mDevice = nullptr;
irr::gui::IGUIEnvironment* IrrlichtGlobals::mGuiEnvironment = nullptr;
irr::video::IVideoDriver* IrrlichtGlobals::mVideoDriver = nullptr;
irr::ITimer* IrrlichtGlobals::mTimer = nullptr;
irr::gui::ICursorControl* IrrlichtGlobals::mCursorControl = nullptr;
irr::scene::ISceneManager* IrrlichtGlobals::mSceneManager = nullptr;
irr::io::IFileSystem* IrrlichtGlobals::mFileSystem = nullptr;
irr::scene::ISceneCollisionManager* IrrlichtGlobals::mCollisionManager = nullptr;

void IrrlichtGlobals::initAll()
{
    mDevice = irr::createDevice(irr::video::EDT_DIRECT3D9, irr::core::dimension2d<uint32_t>(1024, 768), 32, false, false, false, &game::gameManager());
    mGuiEnvironment = mDevice->getGUIEnvironment();
    mVideoDriver = mDevice->getVideoDriver();
    mTimer = mDevice->getTimer();
    mCursorControl = mDevice->getCursorControl();
    mSceneManager = mDevice->getSceneManager();
    mFileSystem = mDevice->getFileSystem();
    mCollisionManager = mSceneManager->getSceneCollisionManager();
}

void IrrlichtGlobals::destroyAll()
{
    mDevice->drop();
}