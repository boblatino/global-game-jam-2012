#pragma once

namespace ai
{
    class PathManager;
}

namespace enemy
{
    class EnemyManager;
}

namespace game
{
    class GamePlayer;
}

namespace item
{
    class ItemManager;
}

namespace weapon
{
    class WeaponManager;
}

namespace utils
{
    class FXManager;
}

class Globals
{
public:
    static void initAll();
    static void destroyAll();

    static utils::FXManager& fxManager() { return *mFXManager; }
    static game::GamePlayer& gamePlayer() { return *mGamePlayer; }

private:
    static utils::FXManager* mFXManager;
    static game::GamePlayer* mGamePlayer;

    Globals();
    Globals(const Globals& globals);
    Globals& operator=(const Globals& globals);
};