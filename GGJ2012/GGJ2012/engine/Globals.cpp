#include "Globals.h"

#include <engine/IrrlichtGlobals.h>

#include <game/EntitiesID.h>
#include <game/GamePlayer.h>

#include <utils/FXManager.h>

utils::FXManager* Globals::mFXManager = nullptr;
game::GamePlayer* Globals::mGamePlayer = nullptr;

void Globals::initAll()
{
    mFXManager = new utils::FXManager();
    mGamePlayer = new game::GamePlayer(game::EntitiesIds::mPlayerId);
}

void Globals::destroyAll()
{
    delete mFXManager;
    delete mGamePlayer;
}